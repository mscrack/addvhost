Скрипт для создания виртуальных хостов.
Запуск скрипта:

Создание хоста
addvhost -h example.dev

Создание хоста с указание директории (как в Yii 2).
addvhost -h example.dev -d /web

Добавляем алиас (как в Yii 2 advanced)
addvhost -h example.dev -d /backend/web -a admin.example.dev
addvhost -h example.dev -d /frontend/web
